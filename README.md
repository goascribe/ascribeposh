# Ascribe API Powershell Interface #

Powershell Cmdlets written in VS 2015 to interface with Ascribe Inspector. A valid Ascribe Intelligence account and license for Ascribe Inspector is required. See goascribe.com for product details.

### Purpose ###
The purpose of this project is twofold.  First, it provides a barebones way to interact with Ascribe Inspector via a command line or scripting without the need for the entire Ascribe User Interface.  Secondly, the code can be used as an example of how to programmatically access the Ascribe API.  

### Usage ###
The binary version of this product is available via the Powershell Gallery.

https://www.powershellgallery.com/packages/Ascribe.Inspector/

Once imported into Powershell, there will be 9 new commands available:

**Start-InspectorSession**

**Get-InspectorResource**

**Add-Inspection**

**Submit-InspectionFileLoad**

**Submit-InspectionGenerateAnswers**

**Submit-InspectionNewAnalysis**

**Submit-InspectionTranslation**

**Submit-InspectionUpdateAnalyses**

**Submit-InspectionNewExport**

You can type Get-Help [Command Name] in Powershell to get relevant usage information about each command.  Also, the commands are meant to allow chaining so you can -- for example -- start an inspector session, create a new inspection, load data, analyze, apply a taxonomy and run an export of the analyzed data all from a single command line action.

For more information on the Ascribe Inspector API, help can be found at the URL below.  The Powershell interface attempts to mirror the variable names in the API specification as closely as possible.

https://webservices.languagelogic.net/Inspector