﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RestSharp;

namespace InspectorCmdlet
{
  public static class InspectorRestClient
  {
    #region Enums

    public enum Environments
    {
      [Description("https://webservices.languagelogic.net/inspector")]
      PROD,
      [Description("https://nr.test.languagelogic.net/inspector")]
      NR,
      //[Description("http://127.0.0.1/inspector")] // Local would be for Internal Ascribe use only
      //Local, 
      [Description("https://cr.test.languagelogic.net/inspector")]
      CR
    }
    public enum Resources
    {
      Exports,
      Inspections,
      Jobs,
      Languages,
      Processors,
      RuleSets,
      Sessions,
      Taxonomies,
      Users
    }
    #endregion

    /// <summary>
    /// Gets a RestClient given an Ascribe environment
    /// </summary>
    /// <param name="Environment">Ascribe Environment (PROD, CR, NR)</param>
    /// <returns>RestClient</returns>
    public static RestClient GetClient(Environments Environment)
    {
      string url = EnumHelper<Environments>.GetEnumDescription(Environment.ToString());
      var client = new RestClient(url);
      return new RestClient(url);
    }

    /// <summary>
    /// Returns a RestRequest object based on input information
    /// </summary>
    /// <param name="token">Authentication Token</param>
    /// <param name="Resource">Which resource (controller) the request is hitting</param>
    /// <param name="Method">POST or GET</param>
    /// <param name="RequestObject">API Request object (optional)</param>
    /// <param name="id">ID to use on querystring (optional)</param>
    /// <returns></returns>
    public static RestRequest GetClientRequest(Guid token, Resources Resource, Method Method, object RequestObject = null, string id = null)
    {
      var clientRequest = new RestRequest();
      // add Ascribe authentication token to header
      clientRequest.AddHeader("authentication", token.ToString());
      clientRequest.Resource = Resource.ToString();

      if (!string.IsNullOrEmpty(id))
      {
        clientRequest.Resource += "/" + id;
      }
      clientRequest.RequestFormat = DataFormat.Json;
      clientRequest.Method = Method;

      // Add request object to body if needed
      if (RequestObject != null)
        clientRequest.AddBody(RequestObject);
      return clientRequest;
    }
  }

  static class EnumHelper<T>
  {
    /// <summary>
    /// Returns the "Description" attribute value on the current Enum
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string GetEnumDescription(string value)
    {
      Type type = typeof(T);
      var name = Enum.GetNames(type).Where(f => f.Equals(value, StringComparison.CurrentCultureIgnoreCase)).Select(d => d).FirstOrDefault();

      if (name == null)
      {
        return string.Empty;
      }
      var field = type.GetField(name);
      var customAttribute = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
      return customAttribute.Length > 0 ? ((DescriptionAttribute)customAttribute[0]).Description : name;
    }
  }
}
