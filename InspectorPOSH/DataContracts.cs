﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static InspectorCmdlet.InspectorRestClient;

namespace Contracts
{
  #region API Requests
  #region New Session  
  public class NewSessionRequest
  {
    public string account;
    public string userName;
    public string password;
  }
  #endregion

  #region New Inspection
  public class NewInspectionRequest
  {
    public string id { get; set; }
    public string description { get; set; }
    public bool isPublic { get; set; }
    public bool allowLink { get; set; }
    public bool preventTakingOwnership { get; set; }
  }
  #endregion

  #region Get Resource
  public class GetResourceRequest
  {
    public string Resource;
  }
  #endregion

  #region POST Job

  #region InspectionJobRequest 
  public class InspectionJobRequest
  {
    public int inspectionKey { get; set; }
    public bool continueOnError { get; set; }
    public bool validateOnly { get; set; }
    public List<FileLoadTaskRequest> fileLoads { get; set; }
    public GenerateAnswersTaskRequest generateAnswers { get; set; }
    public List<NewAnalysisTaskRequest> newAnalyses { get; set; }
    public List<TranslationTaskRequest> translations { get; set; }
    public UpdateAnalysesTaskRequest updateAnalyses { get; set; }
    public List<NewExportTaskRequest> newExports { get; set; }
  }
  #endregion

  #region File Load
  public class FileLoadTaskRequest
  {
    public string uri { get; set; }
    public string ftpUserName { get; set; }
    public string ftpPassword { get; set; }
    public int? ruleSetKey { get; set; }
  }
  #endregion

  #region Generate Answers
  public class GenerateAnswersTaskRequest
  {
    public List<int> questionKeys { get; set; }
  }
  #endregion

  #region New Analysis
  public class NewAnalysisTaskRequest
  {
    public List<int> questionKeys { get; set; }
    public string processorId { get; set; }
    public int? ruleSetKey { get; set; }
    public int? taxonomyKey { get; set; }
  }
  #endregion

  #region Translations
  public class TranslationTaskRequest
  {
    public int sourceQuestionKey { get; set; }
    public string targetQuestionId { get; set; }
    public string targetLanguageId { get; set; }
    public string sourceLanguageId { get; set; }
    public bool targetQuestionMayExist { get; set; }
    public bool useLanguageDetection { get; set; }
    public bool useRLanguageQuestion { get; set; }
  }
  #endregion

  #region UpdateAnalyses
  public class UpdateAnalysesTaskRequest
  {
    public bool updateAll { get; set; }
    public List<int> analysisKeys { get; set; }
  }
  #endregion

  #region NewExport
  public class NewExportTaskRequest
  {
    public string exportTypeId { get; set; }
    public string fileName { get; set; }
    public int? analysisKey { get; set; }
    public bool exportAllAnalyses { get; set; }
  }
  #endregion

  #endregion
  #endregion

  #region API Responses
  public class JobResponse
  {
    public Guid jobGuid { get; set; }
    public InspectionJobStatus status { get; set; }
    public string errors { get; set; }
  }

  #region Job
  public class JobStatusResponse
  {
    public Job job { get; set; }
    public List<string> errors { get; set; }
  }
  public class Job
  {
    public Guid jobGuid { get; set; }
    public int inspectionKey { get; set; }
    public int userKey { get; set; }
    public DateTime startDateUtc { get; set; }
    public DateTime? endDateUtc { get; set; }
    public InspectionJobStatus status { get; set; }
  }
  public class InspectionJobStatus
  {
    public int inspectionKey { get; set; }
    public bool continueOnError { get; set; }
    public bool validateOnly { get; set; }
    public bool hasErrors { get; set; }
    public List<string> jobErrors { get; set; }
    public List<FileLoadTaskStatus> fileLoads { get; set; }
    public GenerateAnswersTaskStatus generateAnswers { get; set; }
    public List<NewAnalysisTaskStatus> newAnalyses { get; set; }
    public List<TranslationTaskStatus> translations { get; set; }
    public UpdateAnalysesTaskStatus updateAnalyses { get; set; }
    public List<NewExportTaskStatus> newExports { get; set; }

  }
  #endregion

  #region Tasks

  #region Base
  public class TaskStatusBase
  {
    public Guid taskGuid { get; set; }
    public int currentPhase { get; set; }
    public int maxPhase { get; set; }
    public decimal percentComplete { get; set; }
    public DateTime? startDateUtc { get; set; }
    public DateTime? endDateUtc { get; set; }
    public List<string> taskErrors { get; set; }
    public bool inspectionLocked { get; set; }

  }
  #endregion

  #region FileLoad
  public class FileLoadTaskStatus : TaskStatusBase
  {
    public int rowsProcessed { get; set; }
    public int runTimeSeconds { get; set; }
    public int questionsAdded { get; set; }
    public int respondentsAdded { get; set; }
    public int responsesLoaded { get; set; }
    public FileLoadTaskRequest request { get; set; }
  }
  #endregion

  #region GenerateAnswers
  public class GenerateAnswersTaskStatus : TaskStatusBase
  {
    public GenerateAnswersTaskRequest request { get; set; }

  }
  #endregion

  #region NewAnalysis
  public class NewAnalysisTaskStatus : TaskStatusBase
  {
    public NewAnalysisTaskRequest request { get; set; }

  }
  #endregion

  #region Translation
  public class TranslationTaskStatus : TaskStatusBase
  {
    public TranslationTaskRequest request { get; set; }
  }
  #endregion

  #region UpdateAnalyses
  public class UpdateAnalysesTaskStatus : TaskStatusBase
  {
    public UpdateAnalysesTaskRequest request { get; set; }
  }
  #endregion

  #region NewExport
  public class NewExportTaskStatus : TaskStatusBase
  {
    public NewExportTaskRequest request { get; set; }
    public string uri { get; set; }
  }
  #endregion
  
  #endregion

  #region Inspection
  public class InspectionResponse
  {
    public InspectionResponse() { }
    public InspectionDetails inspection { get; set; }
    public List<string> errors { get; set; }
  }
  public class InspectionDetails
  {
    public InspectionDetails() { }
    public Dictionary<int, Question> questions { get; set; }
    public Dictionary<int, Analysis> analyses { get; set; }
    public int key { get; set; }
    public string id { get; set; }
    public string description { get; set; }
    public int ownerKey { get; set; }
    public string ownerId { get; set; }
    public bool isPublic { get; set; }
    public bool allowLink { get; set; }
    public bool preventTakingOwnership { get; set; }
    public bool isLocked { get; set; }
    public DateTime utcCreateDate { get; set; }

  }
  public class Question
  {
    public Question() { }
    public int key { get; set; }
    public string id { get; set; }
    public string text { get; set; }
    public bool hasAnswers { get; set; }
    public int countResponses { get; set; }
    public int countDistinctResponses { get; set; }
    public int maximumResponseLength { get; set; }
    public int minimumResponseLength { get; set; }
    public decimal averageResponseLength { get; set; }
    public decimal varianceResponseLength { get; set; }
    public List<string> detectedLanguages { get; set; }
  }
  public class Analysis
  {
    public Analysis() { }
    public int key { get; set; }
    public string processor { get; set; }
    public DateTime startDateUtc { get; set; }
    public DateTime? endDateUtc { get; set; }
    public int? ruleSetKey { get; set; }
    public int? taxonomyKey { get; set; }
    public string description { get; set; }
    public string error { get; set; }

    public bool hasSentiment { get; set; }
    public bool preserveGroups { get; set; }
    public int userKey { get; set; }
  }
  #endregion

  #endregion

  #region Powershell Responses
  public class NewSessionResponse
  {
    public string authenticationToken { get; set; }
    public string errors { get; set; }
  }
  public class TokenResponse
  {
    public TokenResponse(Guid AuthToken, Environments Env)
    {
      this.AuthToken = AuthToken;
      Environment = Env;
      //Chained = true;
    }
    public Guid AuthToken { get; set; }
    public Environments Environment { get; set; }
    //public bool Chained { get; set; }
  }
  public class AddInspectionResponse : TokenResponse
  {
    public AddInspectionResponse(int key, Guid AuthToken, Environments Env) : base(AuthToken, Env)
    {
      this.InspectionKey = key;
    }
    public int InspectionKey { get; set; }

  }
  public class SubmitJobResponse : TokenResponse
  {
    public SubmitJobResponse(int key, Guid JobGuid, Guid AuthToken, Environments Env) : base(AuthToken, Env)
    {
      this.Key = key;
      this.JobGuid = JobGuid;
    }
    public int Key { get; set; }
    public Guid JobGuid { get; set; }
  }
  public class AnalysisResponse : SubmitJobResponse {
    public AnalysisResponse(int key, Guid JobGuid, Guid AuthToken, Environments Env, int AnalysisKey) : base(key, JobGuid, AuthToken, Env)
    {
      this.AnalysisKey = AnalysisKey;
    }
    public int AnalysisKey { get; set; }
  }
  public class NewExportJobResponse : SubmitJobResponse
  {
    public NewExportJobResponse(int key, Guid JobGuid, Guid AuthToken, Environments Env, string ExportFileUri) : base (key, JobGuid, AuthToken, Env) 
    {
      this.ExportFileUri = ExportFileUri;
    }
    public string ExportFileUri { get; set; }
  }
  #endregion

}